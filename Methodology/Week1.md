# Methodology

### Week 1

In the first week, all student has been introduced by Dr Salahuddin as our main instructor for this Aerospace Design Project. This project actually is a same master's project named Putraspace Hybrid Airship UAV (PHSAU), which for us to continue by integrating a specific task of application. A meeting has been made by Dr Salahuddin and Mr Amirul Fiqri to ensure all the student know about the project plan at the early stages. 

There are several subsystems that need to consider in order to make this programs going smoothly as expected. The subsystems will be the group named as the all the student will be divided by following their capabilities and interest. The subsystem teams are:

1. Flight system integration
2. Design 
3. Simulation
4. Propulsion
5. Control Systems
6. Payload (Agricultural Sprayer)

The first objective is to see the control systems really works on the small scale airship, then, after the controls systems is fully developed and tested, there will be a new bigger airship that can carry a payloads which can perform an agricultural spraying. In conclusion, at the end of this semester, there will be two airship with different scale will be fabricated and tested.

Thefore, a flowchart has been made to give an early exposure of developing an aircraft/UAV from scratch:

<div align="middle">

<img src="Assets/IDP_Development_Flowchart-Airship.drawio.png" alt="screenshot" width="800"/>

<div align="left">

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
