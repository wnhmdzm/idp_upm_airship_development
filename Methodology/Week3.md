# Methodology

### Week 3

##### Propulsion Test (Using previous Motor of PHSAU)

A thurst test experiment has been conducted by Propulsion Subsytems Group under supervision Dr Ezanee in the Propulsion Lab H2.3. The main objective of this thurst test experiment is to see whether the motor is still functioning or not, the capabilities of the motor and the esc with a difference length of propeller that has been used before. All the setup has been conducted by using [RCBenchmark](https://www.tytorobotics.com/?utm_source=google&utm_medium=cpc&utm_campaign=discovery&utm_source=google&utm_medium=cpc&utm_campaign=home%20page&gclid=CjwKCAiAvriMBhAuEiwA8Cs5lUkOSH2vxp8fVSaRVbsPQapZWMwMhm9pjp_K4pHo-G_3A1hl-CSg0RoCpnEQAvD_BwE) The specification of the motor, esc and the propeller size is shown below:

| No | Component | Spec |
|---|---|---|
| 1 | Tarot Motor | 320kv |
| 2 | Turningy - Electornics control speed (ESC) | 60A  |
| 3 | Propeller Sizing | 24 inch |


Below is the outcomes for the previous motor (Tarot 320kV) that has been used in PHSAU with 24 inch propeller:

<img src="Assets/WhatsApp_Image_2021-11-12_at_3.43.22_PM.jpeg" alt="screenshot" width="600"/>


From this graph, we can see that the maximum capabilities for Tarot Motor 320kV just at 1450 g Thurst due to the maximum capabilities of the current. After the maximum part, we push the throttle to be higher and suddenly the graph drop extremely due to excessive heat. 

Notes: The application that will be used for the PHAU is only 20% of the thrust. So the graph is very usefull to see the thrust capabilities at 20%.

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)


