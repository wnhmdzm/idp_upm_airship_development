# Methodology

### Week 5

Based on the milestones that has been set up, all the ordered equipments supposed to arrrived on this week. So, Systems Integration groups not really have a jobs to do, but we managed to help others subsystems also such as the control systems and the simulation groups. 

Yana of of the members from the simulation subsystems groups asking us about the parameters to run the simulation by using Ansys. In order to get the Coefficient of Lift and Drag, all the parameters such as the target altitude, pressure, velocity, overall mass and etc need to be known well. so, all the parameters is a new task for us "Systems Intergration Subsystems" to find it by using a Airship Text Book as reference step. All the calculation has been conducted and has been lead by Nash.

For the control subsystems, they manage to meet Mr. Azizi to discuss in deeper regarding the control systems weekly task. The only things that Mr. Azizi told them is to familiarize the function of Flight controller Pixhawk and Mission Planner. Since the project is to update and to make it better in terms of control systems of PSHAU, most of the control systems are ready but need to be tested. The configuration and setup has been made by Mr. Azizi and he want the teams just to know how to use both of the things that he mention earlier. I was there at H2.1 and join the meeting since I was helping Dr. Ermira Junita Abdullah for their project.

Progress in this week actually a bit slow due to the unplanned milestone, and the ordered equipments didnt arrived yet.

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
