# Methodology

### Week 6

Since the equipment still not in the lab, system integration teams as usual is helping another subsystems. The design subsystem is one of the part that we are helping on especially on the gondola design. The issue is on the gondola design attachment, to the airship and the payloads.

Their subsystems has their own idea, and we just give some problems possibilities and some solution for that problems. The main possibilities that they may consider, but not really into it is the easier way to assamble and dis-assamble the gondola to the airship and the gondola to the payloads which are sprayer.

They also get some tips and advice from fiqri and Azizi regarding this problems.

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
