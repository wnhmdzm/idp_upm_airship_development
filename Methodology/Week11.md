# Methodology

### Week 11

#### Test on table and connection picture

here is the picture before test run on table:

<div align="middle">

<img src="Assets/Connection.jpeg" alt="screenshot" width="800"/>

<div align="left">


#### Operating Software Issues (to upload firmware)

there is an error regarding to uploading the firmware:

<div align="middle">

<img src="Assets/errorupload.jpeg" alt="screenshot" width="800"/>

<div align="left">

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
