# Methodology

### Week 2

##### Grouping

In the second week, Dr Salah had already divided us into groups according to our interest and capability. [Personality test](https://richardstep.com/dope-personality-type-quiz/dope-bird-4-personality-types-test-questions-online-version/) had been given to us to determine our preferences. The group table is shown below: 

<div align="middle">

<img src="Assets/IDP_Grouping.png" alt="screenshot" width="600"/>

<div align="left">

It was consists of 7 main groups od Subsystems which are Flight Integration, Control Systems, Propulsion, Design, Simulation and Payload (Sprayer). Then, another teams on the right side is created based on these 7 susbsystems group, which are a combination of students from subsystems group and the task is specified for the progress report, presentation and etc.

This group is created to allow students to understand other tasks and to see overall progress of this project. In order to make it more organized, a table of gantt chart is made based on the milestone target. The first milestone target is to have a first fly test for a medium scale airship on the week of 8 and the second miles stone is to have a flight test for a bigger scale Airship with and without payloads on the week of 12 and 14. There have to spreadsheet has been made after a discussion with all the main groups to fill on between the target milestones.

Below is a link to the spreadsheet table:

<div align="middle">


| No | Name | Owner | Link |
|---|---|---|---|
| 1 | Overview of milestones | Asfeena | [Link](https://docs.google.com/spreadsheets/d/1p-Y4sVX7XehS9pj74TSe7Sm7m5ePHO_Mc-EFzWUNt9c/edit?usp=sharing) |
| 2 | Gantt chart in details | Haikal | [Link](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit?usp=sharing) |

<div align="left">

##### Gitlab Training and tasks

A proper documentation needed to records all the progress. Mr, Azizi Malek has proposed an idea to use GitLab as a based for the progress documentation, and it was was usefull due to easy access, security and good implementation/exposure of Gitlab/Github. Thefore, a meeting has been made by Mr. Azizi Malek as to introduced and give a basic training of GitLab. After the class, a task named task 2 has been assign to do individually in order to see understanding and capability to do the task.

The main objective of this task is to develop a personal profile and to use all the basics __MarkDown Language__

Here is the [Gitlab_IDP_Group_Link](https://gitlab.com/putraspace/idp-2021) and here is my [Task_2](https://gitlab.com/wnhmdzm/my-very-first-project/-/blob/main/main.md) 

##### Equipment Check List and Bill of Material (BOM)

Since this project is a continuation project from "Putraspace Hybrid Airship UAv (PSHAU)", all the equipment, servos and motors already in the labs but the condition might be not very good to reused. Thus, a Flight Systems Integration group's member and several person on the other group was in the lab help on the equipment check. All the activities on the H2.1 lab is under supervision Mr. Fiqri

A spreadsheet named ["Tracker: IDP Equipment List Check and Group Progress"](https://docs.google.com/spreadsheets/d/1ij4VMTHH0KphTnuX29gXc1W_kzoVliqFd0tuFXs60F4/edit#gid=0) is one of the alternative to track all the progress, equipment buying process and etc, that easier for everyone that participating on this project to know what is the status of the equipment.

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
