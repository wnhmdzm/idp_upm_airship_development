# Abstract
<img src="Assets/AirshipAbstract.png" alt="screenshot" width="1400"/>

A project that conducted by final year student to develop and fabricate an [Airship](https://en.wikipedia.org/wiki/Airship) from scratch. The main mission of this Airship is to perform an agricultural spraying. In this project, two airship with different scale will be developed and tested. All of the project development, engineering calculation, fabrication, and testing program are under supervison [Dr. Ahmad Salahuddin Mohd Harithuddin](https://profile.upm.edu.my/a_salahuddin), [Mr. Amirul Fiqri Abdullah](https://www.linkedin.com/in/amirulfiqriabdullah-1425bb18a) and [Mr. Azizi Malik](https://www.linkedin.com/in/azizimalek). 

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
