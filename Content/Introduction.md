# __Introduction__

## Research Background

<img src="Assets/DroneIntroduction.jpeg" width=500 align=middle> 
<img src="Assets/AirshipIntroduction.png" width=435 align=middle> 

The use of unmanned aerial vehicles (UAVs) has gained in popularity in recent years owing to its potential for use in surveillance, exploration, monitoring, and transportation operations. In this regard, efforts have been focused on the development of semi-autonomous vehicles equipped with on-board control and navigation systems capable of planning and executing trajectories with the specific objective of agricultural spraying. This Airship can perform low-speed, low-altitude tasks similar to aircraft and helicopters, which is extremely valuable for the project's intended objective.

There are many engineering applications that help in the agricultural sector, particularly in the aerospace sector, where there are many examples of engineering products that carry out a mission for agricultural needs, such as DJI drones called Agras T20, Agras T30, PrecisionHawk Lancaster 5, and many more with different specific missions. By developing this concept, it is possible that a new agricultural product utilising an airship will be developed, attracting both internal and external investors.

In this Gitlab, all the progress of this project will be reported ahead weekly and checked by associate lecturer; [Dr. Ahmad Salahuddin Mohd Harithuddin](https://profile.upm.edu.my/a_salahuddin), and two other supervisor; [Mr. Amirul Fiqri Abdullah](https://www.linkedin.com/in/amirulfiqriabdullah-1425bb18a) and [Mr. Azizi Malik](https://www.linkedin.com/in/azizimalek).

## Problems Statement

Unmanned Aerial Vehicles (UAVs) are starting to spark an interest within the Malaysia. One of the largest economy sectors taking notice of these machines’ capabilities is the agriculture industry. This is because of their ability to capture real time data/imagery that can be used as viable input into a grower’s decision making process. 

This project is focused on Paddy field crops due to several reason. Ususally, paddy field have a large crops area, and thefore they need more manpower to maintain the crops qualities; pest control and fertilizer needs. This project could help to reduced the number of manpower and at the same time can reduced overall cost. This Airship with the Agricultural Sprayer have different capabilities than a normal Agricultural Drones, which this Airship can carry more loads of the fertilizer/pest controls chemicals, and can cover more crops when performing their task.

## Research Objective

In this project, we have discussed to acheive several objective and the objective are:

1. to determine thrust and propulsion required for 20kg mass of Airship UAV

2. to design and obtain/estimate/predict aerodynamic characteristic for Airship airframe with and without Agricultural Sprayer Load

3. to fabricate and perform a flight test with and without Agricultural Sprayer application

4. to obtain performance prediction for integrated propulsion + airframe Airship UAV (range, endurance, and battery analysis)

## Research Question

In the process of developing and testing the Airship with Agricultural Sprayer UAV, there are several research questions that are important to review. The research question is:

1. What is the weight of the loads for the mission?
2. What is the best mission profile plan by considering the Agricultural Sprayer mission, aerodynamic of Airship airframe and the batery selection?
3. What is the best design and configuration of the Agricultural Sprayer that can fit Airship Design?
4. What is the total flight time of the airship to complete the Agricultural Sprayer mission?
5. What time and weather conditions are needed to run this flight test?

## Scope of Research

This project is focused on the application of Agricultural Sprayer and the development of an Airship. The main objective of this project to test the apllication of Agricultural Sprayer systems as well as the flight control system of an Airship, by taking a flight test at the end of this semester.

Airship with a Agricultural Sprayer is a new project by taking a previous design from "Putraspace Hybrid  Airship UAV (PSHAU)" as a benchmark to compare with, and in this project we are expected to make it batter in term of performance of flying, control systems and their integrated application.

The application has been chosen to do agricultural activities since Malaysia Goverment support agricultural product and development. This Agricultural Sprayer might be usefull for the target industry as well as engineering research and development to attract investors to invest on this project, new student to apply on Degree of Aerospace Engineering UPM and many other opportunities.

All final year student of Aerospace Engineering UPM has been divided into several subsystems such as Flight Integration Team, Design Team, Simulation Team, Software and Control System Team, and Sprayer Team. By this, all the Airship and the application development will be more efficient and easier. All the project are conducted and processed in the Engineering Faculty UPM.

---
Back to [Table of Content](https://gitlab.com/wnhmdzm/idp_upm_airship_development/-/blob/main/README.md)
